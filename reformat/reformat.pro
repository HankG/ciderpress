#-------------------------------------------------
#
# Project created by QtCreator 2015-06-08T21:22:13
#
#-------------------------------------------------

QT       -= core gui

TARGET = reformat
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    AppleWorks.cpp \
    Asm.cpp \
    AWGS.cpp \
    BASIC.cpp \
    Charset.cpp \
    CPMFiles.cpp \
    Directory.cpp \
    Disasm.cpp \
    DisasmTable.cpp \
    DoubleHiRes.cpp \
    DreamGrafix.cpp \
    HiRes.cpp \
    MacPaint.cpp \
    NiftyList.cpp \
    PascalFiles.cpp \
    PrintShop.cpp \
    Reformat.cpp \
    ReformatBase.cpp \
    ResourceFork.cpp \
    Simple.cpp \
    StdAfx.cpp \
    SuperHiRes.cpp \
    Teach.cpp \
    Text8.cpp

HEADERS += \
    AppleWorks.h \
    Asm.h \
    AWGS.h \
    BASIC.h \
    Charset.h \
    CPMFiles.h \
    Directory.h \
    Disasm.h \
    DoubleHiRes.h \
    HiRes.h \
    MacPaint.h \
    PascalFiles.h \
    PrintShop.h \
    Reformat.h \
    ReformatBase.h \
    ResourceFork.h \
    Simple.h \
    StdAfx.h \
    SuperHiRes.h \
    Teach.h \
    Text8.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
