#ifndef CREATEIMAGEDIALOG_H
#define CREATEIMAGEDIALOG_H

#include <QDialog>
#include <QRadioButton>

namespace Ui {
class CreateImageDialog;
}

class CreateImageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateImageDialog(QWidget *parent = 0);
    ~CreateImageDialog();

private slots:
    void diskSizeChanged(int i);
    void diskSizeChanged(QRadioButton* button);
    void fileSystemChanged(int i);
    void on_txtDosTracks_editingFinished();
    void validateBlockSize();
    void validateDosTracks();

private:
    void disableAllItems();
    void toggleDosOptions(bool enabled);
    void toggleDiskSizeOptions(bool enabled);
    void toggleHfsOptions(bool enabled);
    void togglePascalOptions(bool enabled);
    void toggleProdosOptions(bool enabled);

    Ui::CreateImageDialog *ui;
};

#endif // CREATEIMAGEDIALOG_H
