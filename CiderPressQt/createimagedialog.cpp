#include "createimagedialog.h"
#include "ui_createimagedialog.h"

#include <QAbstractButton>
#include <QMessageBox>

CreateImageDialog::CreateImageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateImageDialog)
{
    ui->setupUi(this);
    this->disableAllItems();
}

CreateImageDialog::~CreateImageDialog()
{
    delete ui;
}

void CreateImageDialog::diskSizeChanged(int i)
{
    const QString floppy = "rb140KB";
    const QString microFloppy = "rb800KB";
    const QString hdFloppy = "rbHDFloppy";
    const QString hd5MB = "rb5MB";
    const QString hd16MB = "rb16MB";
    const QString hd20MB = "rb20MB";
    const QString hd32MB = "rb32MB";
    const QString specified = "rbSpecifySize";

    QRadioButton* button = static_cast<QRadioButton*>(ui->bgDiskSize->button(i));
    this->diskSizeChanged(button);

}

void CreateImageDialog::diskSizeChanged(QRadioButton* button)
{
    const QString floppy = "rb140KB";
    const QString microFloppy = "rb800KB";
    const QString hdFloppy = "rbHDFloppy";
    const QString hd5MB = "rb5MB";
    const QString hd16MB = "rb16MB";
    const QString hd20MB = "rb20MB";
    const QString hd32MB = "rb32MB";
    const QString specified = "rbSpecifySize";

    const int floppyBlocks = 280;
    const int microFloppyBlocks = 1600;
    const int hdFloppyBlocks = 2880;
    const int hd5MBBlocks = 10240;
    const int hd16MBBlocks = 32768;
    const int hd20MBBlocks = 40960;
    const int hd32MBblocks = 65535;

    QString name = button->objectName();
    QLineEdit* text = ui->txtBlockSize;

    text->setEnabled(false);

    if(QString::compare(name, floppy)==0)
    {
        text->setText(QString::number(floppyBlocks));
    }
    else if(QString::compare(name, microFloppy)==0)
    {
        text->setText(QString::number(microFloppyBlocks));
    }
    else if(QString::compare(name, hdFloppy)==0)
    {
        text->setText(QString::number(hdFloppyBlocks));
    }
    else if(QString::compare(name, hd5MB)==0)
    {
        text->setText(QString::number(hd5MBBlocks));
    }
    else if(QString::compare(name, hd16MB)==0)
    {
        text->setText(QString::number(hd16MBBlocks));
    }
    else if(QString::compare(name, hd20MB)==0)
    {
        text->setText(QString::number(hd20MBBlocks));
    }
    else if(QString::compare(name, hd32MB)==0)
    {
        text->setText(QString::number(hd32MBblocks));
    }
    text->setEnabled(false);

    if(QString::compare(name,specified) == 0)
    {
        text->setEnabled(true);
    }
}


void CreateImageDialog::fileSystemChanged(int i)
{
    const QString dos32Name = "rbDos32";
    const QString dos33Name = "rbDos33";
    const QString prodosName = "rbProdos";
    const QString hfsName = "rbHfs";
    const QString blankName = "rbBlank";
    const QString pascalName = "rbPascal";

    QRadioButton* button = static_cast<QRadioButton*>(ui->bgFilesystem->button(i));
    QString name = button->objectName();

    this->disableAllItems();
    ui->rb140KB->setChecked(true);
    this->diskSizeChanged(ui->rb140KB);

    if(QString::compare(name,dos32Name,Qt::CaseInsensitive)==0 || QString::compare(name, dos33Name, Qt::CaseInsensitive)==0)
    {
        ui->rb140KB->setEnabled(true);
        this->toggleDosOptions(true);
    }
    else if (QString::compare(name, prodosName, Qt::CaseInsensitive)==0)
    {
        this->toggleDiskSizeOptions(true);
        this->toggleProdosOptions(true);
    }
    else if (QString::compare(name, pascalName, Qt::CaseInsensitive)==0)
    {
        this->togglePascalOptions(true);
        ui->rb140KB->setEnabled(true);
        ui->rb800KB->setEnabled(true);
    }
    else if (QString::compare(name, hfsName, Qt::CaseInsensitive)==0)
    {
        this->toggleHfsOptions(true);
        this->toggleDiskSizeOptions(true);
        ui->rb140KB->setEnabled(false);
    }
    else if (QString::compare(name, blankName, Qt::CaseInsensitive)==0)
    {
        this->toggleDiskSizeOptions(true);
    }

    ui->txtBlockSize->setEnabled(false);

}


void CreateImageDialog::on_txtDosTracks_editingFinished()
{
    this->validateDosTracks();
}

void CreateImageDialog::validateBlockSize()
{

}

void CreateImageDialog::validateDosTracks()
{
    bool ok = false;
    int trackCount = ui->txtDosTracks->text().toInt(&ok);
    int defaultTrackCount = 254;

    if(!ok)
    {
        QMessageBox::critical(this,tr("Error"), tr("DOS Track count needs to be a number"));
        ui->txtDosTracks->setText(QString::number(defaultTrackCount));
    }

    if (trackCount < 18*8 || trackCount > 800 ||
         (trackCount <= 400 && (trackCount % 8) != 0) ||
         (trackCount > 400 && (trackCount % 16) != 0))
    {
        QMessageBox::critical(this,tr("Error"),tr("DOS Track count block is out of range"));
        ui->txtDosTracks->setText(QString::number(defaultTrackCount));
    }

}


void CreateImageDialog::disableAllItems()
{
    this->toggleDiskSizeOptions(false);
    this->toggleDosOptions(false);
    this->toggleProdosOptions(false);
    this->togglePascalOptions(false);
    this->toggleHfsOptions(false);
}

void CreateImageDialog::toggleDosOptions(bool enabled)
{
    ui->chkAllocateDosTracks->setEnabled(enabled);
    ui->txtDosTracks->setEnabled(enabled);
}

void CreateImageDialog::toggleDiskSizeOptions(bool enabled)
{
    QList<QAbstractButton*> buttons = ui->bgDiskSize->buttons();

    for(int i = 0 ; i < buttons.size(); i++)
    {
        buttons[i]->setEnabled(enabled);
    }

    ui->txtBlockSize->setEnabled(enabled);
}

void CreateImageDialog::toggleHfsOptions(bool enabled)
{
    ui->txtHfsVolumeName->setEnabled(enabled);
}

void CreateImageDialog::togglePascalOptions(bool enabled)
{
    ui->txtPascalVolumeName->setEnabled(enabled);
}

void CreateImageDialog::toggleProdosOptions(bool enabled)
{
    ui->txtProDosVolumeName->setEnabled(enabled);
}

