#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T14:06:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CiderPressQt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    createimagedialog.cpp

HEADERS  += mainwindow.h \
    createimagedialog.h

FORMS    += mainwindow.ui \
    createimagedialog.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_GCC_64bit-Debug/release/ -lzlib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_MSVC2010_OpenGL_32bit-Debug/debug/ -lzlib
else:unix: LIBS += -L$$PWD/../bin/GCC_64bit-Debug/zlib/ -lzlib

INCLUDEPATH += $$PWD/../bin/GCC_64bit-Debug/zlib
DEPENDPATH += $$PWD/../bin/GCC_64bit-Debug/zlib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_GCC_64bit-Debug/release/libzlib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_GCC_64bit-Debug/debug/libzlib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_GCC_64bit-Debug/release/zlib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../zlib/build-zlib-Desktop_Qt_5_4_2_MSVC2010_OpenGL_32bit-Debug/debug/zlib.lib
else:unix: PRE_TARGETDEPS += $$PWD/../bin/GCC_64bit-Debug/zlib/libzlib.a

DISTFILES +=
