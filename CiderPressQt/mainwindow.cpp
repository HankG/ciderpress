#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "createimagedialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionDisk_image_triggered()
{
    CreateImageDialog createDialog(this);
    createDialog.exec();
}
