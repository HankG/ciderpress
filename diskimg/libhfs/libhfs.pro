#-------------------------------------------------
#
# Project created by QtCreator 2015-06-08T21:16:50
#
#-------------------------------------------------

QT       -= core gui

DEFINES += HAVE_CONFIG_H
TARGET = libhfs
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes
QMAKE_CFLAGS_DEBUG += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes -Wno-pointer-sign -Wno-sign-compare -Wno-unused-label -Wno-unused-parameter

SOURCES += \
    block.c \
    btree.c \
    data.c \
    file.c \
    hfs.c \
    low.c \
    medium.c \
    memcmp.c \
    node.c \
    os.c \
    record.c \
    version.c \
    volume.c

HEADERS += \
    apple.h \
    block.h \
    btree.h \
    config.h \
    data.h \
    file.h \
    hfs.h \
    libhfs.h \
    low.h \
    medium.h \
    node.h \
    os.h \
    record.h \
    version.h \
    volume.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
