#-------------------------------------------------
#
# Project created by QtCreator 2015-06-08T21:10:27
#
#-------------------------------------------------

QT       -= core gui

TARGET = diskimg
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes -Wno-unused-parameter -Wno-missing-field-initializers -Wno-type-limits -Wno-unused-result
QMAKE_CFLAGS_DEBUG += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes -Wno-unused-parameter

unix {
    target.path = /usr/lib
    INSTALLS += target
}

HEADERS += \
    ASPI.h \
    CP_ntddscsi.h \
    CP_WNASPI32.H \
    DiskImg.h \
    DiskImgDetail.h \
    DiskImgPriv.h \
    GenericFD.h \
    SCSIDefs.h \
    SPTI.h \
    StdAfx.h \
    TwoImg.h \
    Win32BlockIO.h \
    Win32Extra.h

SOURCES += \
    ASPI.cpp \
    CFFA.cpp \
    Container.cpp \
    CPM.cpp \
    DDD.cpp \
    DiskFS.cpp \
    DiskImg.cpp \
    DIUtil.cpp \
    DOS33.cpp \
    DOSImage.cpp \
    FAT.cpp \
    FDI.cpp \
    FocusDrive.cpp \
    GenericFD.cpp \
    Global.cpp \
    Gutenberg.cpp \
    HFS.cpp \
    ImageWrapper.cpp \
    MacPart.cpp \
    MicroDrive.cpp \
    Nibble.cpp \
    Nibble35.cpp \
    OuterWrapper.cpp \
    OzDOS.cpp \
    Pascal.cpp \
    ProDOS.cpp \
    RDOS.cpp \
    SPTI.cpp \
    StdAfx.cpp \
    TwoImg.cpp \
    UNIDOS.cpp \
    VolumeUsage.cpp \
    Win32BlockIO.cpp
