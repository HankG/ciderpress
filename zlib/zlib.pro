#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T13:59:37
#
#-------------------------------------------------

QT       -= core gui

TARGET = zlib
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes
QMAKE_CFLAGS_DEBUG += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes -Wno-implicit-function-declaration

SOURCES += \
    adler32.c \
    compress.c \
    crc32.c \
    deflate.c \
    gzclose.c \
    gzlib.c \
    gzread.c \
    gzwrite.c \
    infback.c \
    inffast.c \
    inflate.c \
    inftrees.c \
    trees.c \
    uncompr.c \
    zutil.c

HEADERS += zlib.h \
    crc32.h \
    deflate.h \
    gzguts.h \
    inffast.h \
    inffixed.h \
    inflate.h \
    inftrees.h \
    trees.h \
    zconf.h \
    zutil.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
