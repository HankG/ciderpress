#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T14:56:58
#
#-------------------------------------------------

QT       -= core gui

DEFINES += HAVE_CONFIG_H
TARGET = nufxlib
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes
QMAKE_CFLAGS_DEBUG += -g -O2 -Wall -Wwrite-strings -Wpointer-arith -Wshadow -Wstrict-prototypes -Wno-implicit-function-declaration -Wno-unused-parameter -Wno-sign-compare


SOURCES += Archive.c \
    ArchiveIO.c \
    Bzip2.c \
    Charset.c \
    Compress.c \
    Crc16.c \
    Debug.c \
    Deferred.c \
    Deflate.c \
    Entry.c \
    Expand.c \
    FileIO.c \
    Funnel.c \
    Lzc.c \
    Lzw.c \
    MiscStuff.c \
    MiscUtils.c \
    Record.c \
    SourceSink.c \
    Squeeze.c \
    Thread.c \
    Value.c \
    Version.c

HEADERS += MiscStuff.h \
    NufxLib.h \
    NufxLibPriv.h \
    SysDefs.h \
    config.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
